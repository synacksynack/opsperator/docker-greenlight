FROM docker.io/ruby:2.7.6-alpine AS base

# Set a variable for the install location.
ARG RAILS_ROOT=/usr/src/app
# Set Rails environment.
ENV RAILS_ENV production
ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"

# Make the directory and set as working.
RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

ARG BUILD_PACKAGES="build-base curl-dev git shared-mime-info"
ARG DEV_PACKAGES="postgresql-dev sqlite-libs sqlite-dev yaml-dev zlib-dev nodejs yarn"
ARG RUBY_PACKAGES="tzdata"

# Install app dependencies.
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES

COPY Gemfile* ./
COPY Gemfile Gemfile.lock $RAILS_ROOT/

RUN bundle config --global frozen 1 \
    && bundle config set deployment 'true' \
    && bundle config set without 'development:test:assets' \
    && bundle install -j4 --path=vendor/bundle \
    && rm -rf vendor/bundle/ruby/2.7.0/cache/*.gem \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.c" -delete \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.o" -delete

# Adding project files.
COPY . .

# Remove folders not needed in resulting image
RUN rm -rf tmp/cache spec

############### Build step done ###############

FROM docker.io/ruby:2.7.6-alpine

# Set a variable for the install location.
ARG RAILS_ROOT=/usr/src/app
ARG PACKAGES="tzdata curl postgresql-client sqlite-libs yarn nodejs bash gcompat ca-certificates"
ENV VERSION_CODE=2.12.6 \
    RAILS_ENV=production \
    BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"

LABEL io.k8s.description="Greenlight - BigBlueButton Frontend" \
      io.k8s.display-name="Greenlight" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="greenlight,bigbluebutton" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-greenlight" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION_CODE"

WORKDIR $RAILS_ROOT

COPY --from=base $RAILS_ROOT $RAILS_ROOT

RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $PACKAGES \
    && update-ca-certificates \
    && wget https://curl.se/ca/cacert.pem https://curl.se/ca/cacert.pem.sha256 -P /tmp \
    && ( cd /tmp && sha256sum cacert.pem >cacert.pem.sha256sum ) \
    && ( cd $RAILS_ROOT && mv -v /tmp/cacert.pem $(bundle info httpclient --path)/lib/httpclient/ ) \
    && rm -v /tmp/cacert* \
    && chown -R 1000:0 /etc/ssl /usr/local/share/ca-certificates $RAILS_ROOT \
    && chmod -R g=u /etc/ssl /usr/local/share/ca-certificates $RAILS_ROOT

# Start the application.
CMD ["bin/start"]
USER 1000
