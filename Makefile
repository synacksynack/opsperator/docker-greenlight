SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/greenlight
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demo
demo:
	docker rm -f testgl && sleep 2 || echo OK
	rm -fr ./run
	mkdir -p run/tmp run/db run/log run/storage run/node_modules run/public/assets
	chmod 777 run/tmp run/db run/log run/storage run/node_modules run/public/assets
	docker run \
	    -p 5000:5000 \
	    -e DB_ADAPTER=sqlite3 \
	    -e HOME=/tmp \
	    -e PORT=5000 \
	    -e RAILS_ENV=production \
	    -e SECRET_KEY_BASE=secretsecretsecretsecretkeybase \
	    -v `pwd`/run/public/assets:/usr/src/app/public/assets \
	    -v `pwd`/run/node_modules:/usr/src/app/node_modules \
	    -v `pwd`/run/storage:/usr/src/app/storage \
	    -v `pwd`/run/db:/usr/src/app/db/production \
	    -v `pwd`/run/log:/usr/src/app/log \
	    -v `pwd`/run/tmp:/usr/src/app/tmp \
	    --name testgl -d $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap service secret deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "GREENLIGHT_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "GREENLIGHT_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
